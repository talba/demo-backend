ARG php_version=7.1
FROM php:${php_version}-fpm

# Set up timezone
RUN apt-get -yqq update && apt-get -yqq install tzdata
RUN rm /etc/localtime
ARG timezone=America/Sao_Paulo
RUN ln -sv /usr/share/zoneinfo/${timezone} /etc/localtime
RUN echo "${timezone}" > /etc/timezone

# Set up locales
RUN apt-get -yqq update && apt-get -yqq install locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen


# Install MariaDB
RUN apt-get -yqq update && \
 apt-get -yqq install software-properties-common dirmngr
RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xF1656F24C74CD1D8
RUN add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirror.ufscar.br/mariadb/repo/10.3/debian stretch main'
RUN apt-get -yqq update && \
 apt-get -yqq install mariadb-client --no-install-recommends
RUN sh -c "docker-php-ext-install -j$(nproc) pdo_mysql"

# Install Redis
RUN pecl install redis && \
 docker-php-ext-enable redis
 
# Install Imagick (pending)


# Give project ownership to user www-data 
WORKDIR /var/www
COPY . ./
RUN chown -R www-data:www-data ./
RUN chmod 755 ./