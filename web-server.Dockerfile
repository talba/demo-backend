ARG nginx_version=1.15
FROM nginx:${nginx_version}

# Copy vhost config
COPY .docker/vhost.conf /etc/nginx/conf.d/default.conf

# Set /var/www as workdir
WORKDIR /var/www